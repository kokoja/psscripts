
#Create naming for the export
$day = Get-Date -Format "MM_dd_yyyy"
$hour = Get-Date -Format "h_m" 
$name = "$day"+"_"+"$hour" 

$file1 = "C:\Users\furnadzh\Documents\capacity\ExchangeCalc8.xlsm" # source's fullpath
$file2 = "C:\Users\furnadzh\Documents\capacity\RoleRequirements_$name.csv"# destination's fullpath

#Create and get my Excel Obj
$excel = New-Object -comobject Excel.Application
$excel.visible=$false
$excel.DisplayAlerts=$false
$calculator = $excel.Workbooks.Open($file1)

#Select Role Requirements Sheet
$rolereq = $calculator.Worksheets.Item(3)
$rolereq.Activate()
# $rolereq.Copy()


#Save, close, and clean up
$rolereq.SaveAs($file2, 6)
$calculator.close()
$excel.quit()
$excel = $null
